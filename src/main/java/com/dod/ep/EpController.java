package com.dod.ep;

    import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EpController {
    @RequestMapping(path = "/index")
    public String sample() {
        return "Sample";
    }
}
